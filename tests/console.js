const { OpenSprinklerPi } = require('../dist/index');

const numberZones = process.argv[2];
const controller = new OpenSprinklerPi(numberZones);

console.log('Zone number, r to reset, or x to exit')
var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.on('line', function(line){
  if(line == 'x') {
    process.exit(0);
  }

  if(line == 'r') {
    console.log('Resetting controller');
    controller.reset();
  } else {
    const zone = parseInt(line);

    if(zone >= 0 && zone < numberZones) {
      console.log(`Opening valve for Zone ${line}`);
      controller.openValve(zone);
    }
    else {
      console.log('Invalid zone number');
    }
  }
});
