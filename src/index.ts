import { Gpio, BinaryValue } from 'onoff';
import { ValveController } from '@springfield/valve-controller';
import { Registry, Gauge } from 'prom-client';
import moment from 'moment';

export class OpenSprinklerPi implements ValveController {
  private valveState: BinaryValue[];
  private gauge: Gauge;
  private shiftRegisterClockPin: Gpio;
  private shiftRegisterOutputEnablePin: Gpio;
  private shiftRegisterDataPin: Gpio;
  private shiftRegisterLatchPin: Gpio;

  constructor(numberZones: number, registry: Registry) {
    this.valveState = [];
    this.gauge = new Gauge({name: "open_sprinkler_pi", help: 'Valve status by zone.  1 = valve open; 0 = valve closed', labelNames: ['zone'], registers: [registry]});

    for(let i = 0; i < numberZones; i++) {
      this.valveState.push(0);
    }

    this.shiftRegisterClockPin = new Gpio(4, 'out');
    this.shiftRegisterOutputEnablePin = new Gpio(17, 'out');

    this.shiftRegisterOutputEnablePin.writeSync(1); // Disable the shift register OE

    this.shiftRegisterDataPin = new Gpio(27, 'out');
    this.shiftRegisterLatchPin = new Gpio(22, 'out');

    this.updateShiftRegister();
    this.shiftRegisterOutputEnablePin.writeSync(0); // Enable the shift register OE
  }

  public reset() {
    this.valveState.fill(0);
    this.updateShiftRegister();
  }

  public getValveState(): number[] {
    return this.valveState.slice();
  }

  public openValve(zone: number) {
    this.valveState[zone] = 1;
    const sampleTime = moment().milliseconds(0);
    this.gauge.set({ zone }, 1, sampleTime.valueOf());
    this.updateShiftRegister();
  }

  public closeValve(zone: number) {
    this.valveState[zone] = 0;
    const sampleTime = moment().milliseconds(0);
    this.gauge.set({ zone }, 0, sampleTime.valueOf());
    this.updateShiftRegister();
  }

  private updateShiftRegister() {
    this.shiftRegisterClockPin.writeSync(0);
    this.shiftRegisterLatchPin.writeSync(0);

    for (let i = this.valveState.length - 1; i >= 0; i--)
    {
      this.shiftRegisterClockPin.writeSync(0);
      this.shiftRegisterDataPin.writeSync(this.valveState[i]);
      this.shiftRegisterClockPin.writeSync(1);
    }

    this.shiftRegisterLatchPin.writeSync(1);
  }
}